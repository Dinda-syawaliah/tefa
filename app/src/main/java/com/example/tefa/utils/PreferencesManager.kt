package com.example.tefa.utils

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit

class PreferencesManager constructor(context : Context) {
    private var pref: SharedPreferences
    private var editor: SharedPreferences.Editor

    init {
        pref = context.getSharedPreferences("AppsSharedPref", Context.MODE_PRIVATE)
        editor = pref.edit()
        editor.apply()
    }

    var isFirstOpen: Boolean?
        get() {
            val isFirst = pref.getBoolean("isFirstOpen", true)
            if (isFirst) {
                editor.putBoolean("isFirstOpen", false)?.apply()
            }
            return isFirst
        }
        set(value) {
            if (value != null) {
                editor.putBoolean("isFirstOpen", value)?.apply()
            }
        }
}


